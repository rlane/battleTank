# Battle Tank

A simple third person tank game with mortars.

## Concept 

Drive a tank around a terrain map using "fly by wire" controls similar to those found in the game "World of Tanks."  The tank will battle against another tank in the map which will be controlled by either the AI or an opposing player. Fighthing will occur with a mortar gun equipped on each tank which fires explosive mortar rounds that damage the other tank on impact.
 
## Gameplay
 - Players have finite ammo and health
 - Kill the other player(s) to get a point
 - Kill the other tank by doing enough damage 
 - Most points in time limit wins the game
 - Tanks spawn in random locations and respawn after a delay upon death
 - Shooting the mortar should enforce a delay until the next shot can be taken, and the mortar should have a blast radius
