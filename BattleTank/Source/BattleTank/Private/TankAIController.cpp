// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "TankAIController.h"
#include "TankAimingComponent.h"
#include "Tank.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		ATank *PossesedTank = Cast<ATank>(InPawn);
		if (ensure(PossesedTank))
		{
			PossesedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnDeath);
		}
	}
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	APawn* PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	APawn* ControlledTank = GetPawn();
	if (PlayerPawn != nullptr && ControlledTank != nullptr)
	{
		MoveToActor(PlayerPawn, AcceptanceRadius);
		AimingComponent->AimAt(PlayerPawn->GetActorLocation());
		if (AimingComponent->GetFiringState() == EFiringStatus::Locked)
		{
			AimingComponent->Fire();
		}
	}
}

void ATankAIController::OnDeath()
{
	if (GetPawn() != nullptr)
	{
		GetPawn()->DetachFromControllerPendingDestroy();
	}
}