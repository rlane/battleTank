// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "TankTurret.h"

void UTankTurret::Rotate(float RelativeSpeed) 
{
	// Clamp the speed, calculate the rotatoin factor and add to the current value.  
	// Set the rotation to this result.
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, +1);
	float RotationSpeed = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->GetDeltaSeconds();
	float RawNewRotation = RelativeRotation.Yaw + RotationSpeed;
	SetRelativeRotation(FRotator(0, RawNewRotation, 0));
}