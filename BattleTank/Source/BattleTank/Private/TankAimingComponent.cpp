// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "TankAimingComponent.h"
#include "Projectile.h"

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true; 
}

void UTankAimingComponent::BeginPlay() 
{
	Super::BeginPlay();
	LastFireTime = FPlatformTime::Seconds();
}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	double CurrentTime = FPlatformTime::Seconds();
	if (Ammo <= 0) 
	{
		Status = EFiringStatus::OutOfAmmo;
	}
	else if ((CurrentTime - LastFireTime) < ReloadTimeInSeconds)
	{
		Status = EFiringStatus::Reloading;
	}
	else if (IsBarrelMoving()) 
	{
		Status = EFiringStatus::Aiming;
	}
	else 
	{
		Status = EFiringStatus::Locked;
	}
}

void UTankAimingComponent::Initialise(UTankBarrel *BarrelToSet, UTankTurret *TurretToSet)
{
	Barrel = BarrelToSet;
	Turret = TurretToSet;
}

// Aim at something
void UTankAimingComponent::AimAt(FVector HitLocation) 
{
	if (ensure(Barrel)) 
	{
		FVector OutLaunchVelocity;
		FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));

		// Calculate theOutLaunchVelocity
		bool bHaveAimSolution = UGameplayStatics::SuggestProjectileVelocity
		(
			this,
			OutLaunchVelocity,
			StartLocation,
			HitLocation,
			LaunchSpeed,
			false,
			0,
			0,
			ESuggestProjVelocityTraceOption::DoNotTrace
		);

		float Time = GetWorld()->GetTimeSeconds();
		FString TankName = Barrel->GetOwner()->GetName();
		if (bHaveAimSolution)
		{
			AimDirection = OutLaunchVelocity.GetSafeNormal();
			MoveBarrelTowards(AimDirection);
		}
	}
}

//Move the barrel to match the aiming
void UTankAimingComponent::MoveBarrelTowards(FVector NewDirection)
{
	if (ensure(Barrel && Turret)) 
	{
		// Determine the angle to rotate based on AimDirection
		FRotator BarrelRotator = Barrel->GetForwardVector().Rotation();
		FRotator AimAsRotator = NewDirection.Rotation();
		FRotator DeltaRotator = AimAsRotator - BarrelRotator;

		Barrel->Elevate(DeltaRotator.Pitch);
		//If YAW is more then 180 degrees take the short route.
		if (FMath::Abs(DeltaRotator.Yaw) < 180)
		{
			Turret->Rotate(DeltaRotator.Yaw);
		}
		else
		{
			Turret->Rotate(-DeltaRotator.Yaw);
		}
		
	}
}

void UTankAimingComponent::Fire()
{
	if (Status == EFiringStatus::Locked || Status == EFiringStatus::Aiming)
	{
		if (ensure(Barrel) && ensure(ProjectileBlueprint))
		{
			// Spawn a projectile at the socket of the barrel.
			AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(
				ProjectileBlueprint,
				Barrel->GetSocketLocation(FName("Projectile")),
				Barrel->GetSocketRotation(FName("Projectile")));

			Projectile->LaunchProjectile(LaunchSpeed);
			LastFireTime = FPlatformTime::Seconds();
			Ammo -= 1;
		}
	}
}

int32 UTankAimingComponent::GetAmmo() const
{
	return Ammo;
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(Barrel)) { return false; }
	FVector BarrelForward = Barrel->GetForwardVector();
	return !BarrelForward.Equals(AimDirection, 0.01);
}

EFiringStatus UTankAimingComponent::GetFiringState() const
{
	return Status;
}