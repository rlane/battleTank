// Copyright ShinyLane

#pragma once

#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class ATank;
class UTankAimingComponent;

/**
 * Responsible for helping the player aim.
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void FoundAimingComponent(UTankAimingComponent* AimCompRef);

private:
	UPROPERTY(EditDefaultsOnly)
	float CrossHairXLocation = 0.5;

	UPROPERTY(EditDefaultsOnly)
	float CrossHairYLocation = 0.33333;

	UPROPERTY(EditDefaultsOnly)
	int32 LineTraceRange = 10000;

	UFUNCTION()
	void OnDeath();

	virtual void SetPawn(APawn* InPawn) override;

	// Start the tank moving the barrel so that shot would hit where 
	// the crosshair intersects the world.
	void AimTowardsCrosshair();

	// Cast a ray from the player crosshair and return true if the ray
	// hits anything.  HitLocation is an out variable that will contain
	// the location in the world that was hit by the ray.
	bool GetSightRayHitLocation(FVector &HitLocation) const;

	// Get the look direction of the player camera 
	bool GetLookDirection(FVector2D ScreenLocation, FVector &LookDirection) const;

	bool GetLookVectorHitLocation(FVector LookDirection, FVector &HitLocation) const;
};
