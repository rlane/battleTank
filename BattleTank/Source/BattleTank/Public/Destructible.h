// Copyright ShinyLane

#pragma once

#include "GameFramework/Pawn.h"
#include "Destructible.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathBroadcast);

UCLASS()
class BATTLETANK_API ADestructible : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADestructible();

	// Returns the actual damage given to the component
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	// Returns current health as a percentage of starting health between 0 and 1
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthPercent() const;

	// Subject broadcast for on death
	FDeathBroadcast OnDeath;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	int32 StartingHealth = 100;

	UPROPERTY(VisibleAnywhere, Category = "Health")
	int32 CurrentHealth = StartingHealth;
	
	
};
