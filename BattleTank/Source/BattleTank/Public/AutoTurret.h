// Copyright ShinyLane

#pragma once

#include "GameFramework/Pawn.h"
#include "AutoTurret.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAutoTurretBroadcast);

UCLASS()
class BATTLETANK_API AAutoTurret : public APawn
{
	GENERATED_BODY()

public:
	FAutoTurretBroadcast OnDeath;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	// Returns current health as a percentage of starting health between 0 and 1
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthPercent() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Sets default values for this pawn's properties
	AAutoTurret();
	
	UPROPERTY(EditDefaultsOnly, Category = "Health")
	int32 StartingHealth = 100;

	UPROPERTY(VisibleAnywhere, Category = "Health")
	int32 CurrentHealth = StartingHealth;	
};
